package pl.edu.pwsztar.service;

@FunctionalInterface
public interface Converter<T, F> {
    T convert(F from);
}

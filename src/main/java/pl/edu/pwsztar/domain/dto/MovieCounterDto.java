package pl.edu.pwsztar.domain.dto;

public class MovieCounterDto {

    private final long counter;

    private MovieCounterDto(Builder builder) {
        counter = builder.counter;
    }

    public long getCounter() {
        return counter;
    }

    public static class Builder {
        private long counter;

        public Builder() {
        }

        public Builder counter(Long counter) {
            this.counter = counter;
            return this;
        }

        public MovieCounterDto build() {
            return new MovieCounterDto(this);
        }
    }

            @Override
    public String toString() {
        return "MovieCounterDto{" +
                "counter=" + counter +
                '}';
    }
}

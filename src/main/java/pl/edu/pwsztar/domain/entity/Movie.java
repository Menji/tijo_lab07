package pl.edu.pwsztar.domain.entity;

import pl.edu.pwsztar.domain.dto.DetailsMovieDto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "movies")
public class Movie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private Long movieId;

    @Column(name = "title")
    private String title;

    @Column(name = "image")
    private String image;

    @Column(name = "year")
    private Integer year;

    @Column(name = "video_id")
    private String videoId;

    public Movie () {}

    private Movie(Builder builder) {
        title = builder.title;
        videoId = builder.videoId;
        image = builder.image;
        year = builder.year;
        movieId = builder.movieId;
    }

    public Movie copy(Builder builder) {
        title = builder.title;
        videoId = builder.videoId;
        image = builder.image;
        year = builder.year;
        movieId = builder.movieId;

        return this;
    }

    public static class Builder {
        private String title;
        private String videoId;
        private String image;
        private Integer year;
        private Long movieId;

        public Builder() {
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder image(String image) {
            this.image = image;
            return this;
        }

        public Builder year(Integer year) {
            this.year = year;
            return this;
        }

        public Builder videoId(String videoId) {
            this.videoId = videoId;
            return this;
        }

        public Builder movieId(Long movieId) {
            this.movieId = movieId;
            return this;
        }

        public Movie build() {
            return new Movie(this);
        }
    }

    public Long getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public Integer getYear() {
        return year;
    }

    public String getVideoId() {
        return videoId;
    }
}
